import { callApi } from '../helpers/apiHelper';
import { IFighter } from '../interfaces/IHtmlElement';


export async function getFighters(): Promise<IFighter | IFighter[]> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult: IFighter | IFighter[] = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}


export async function getFighterDetails(id: string): Promise<IFighter | IFighter[]> {
  try {
    const endpoint: string = `details/fighter/${id}.json`;
    const apiResult: IFighter | IFighter[] = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

