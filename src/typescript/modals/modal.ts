import { createElement } from '../helpers/domHelper';


export function showModal(title: string, bodyElement: HTMLDivElement): void {
  const root: HTMLElement | null = getModalContainer();
  const modal: HTMLDivElement= createModal(title, bodyElement); 
  
  root?.append(modal);
}


function getModalContainer(): HTMLElement | null {
  return document.getElementById('root');
}


function createModal(title: string, bodyElement: HTMLDivElement): HTMLDivElement {

  const layer: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-layer' }) as HTMLDivElement;
  const modalContainer: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-root' }) as HTMLDivElement;
  const header: HTMLDivElement = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}


function createHeader(title: string): HTMLDivElement{

  const headerElement: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-header' }) as HTMLDivElement;
  const titleElement: HTMLSpanElement = createElement({ tagName: 'span' }) as HTMLSpanElement;
  const closeButton: HTMLDivElement = createElement({ tagName: 'div', className: 'close-btn' }) as HTMLDivElement;
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  
  return headerElement;
}


function hideModal(event: Event): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
