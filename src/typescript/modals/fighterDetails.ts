import { createElement } from '../helpers/domHelper';
import { IFighter } from '../interfaces/IHtmlElement';
import { showModal } from './modal';



export  function showFighterDetailsModal(fighter: IFighter): void {
  const title: string= 'Fighter info';
  const bodyElement: HTMLDivElement = createFighterDetails(fighter);

  showModal(title, bodyElement);
}



function createFighterDetails(fighter: IFighter): HTMLDivElement {
  const { name, health, attack, defense, source } = fighter;
  
  const fighterDetails: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-body' }) as HTMLDivElement;
  const nameElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-name' }) as HTMLSpanElement;
  const healthElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-health' }) as HTMLSpanElement;
  const attackElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-attack' }) as HTMLSpanElement;
  const defenseElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-defense' }) as HTMLSpanElement;
  const imageElement: HTMLImageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: [{ key: "src", value: source }] }) as HTMLImageElement;

  let roundHealth: number = Math.round(health as number);

  nameElement.innerText = name;
  healthElement.innerText =  `Health:  ${<string>(roundHealth.toString())}`;
  attackElement.innerText =  `Attack:   ${<string>(attack?.toString())}`;
  defenseElement.innerText = `Defense: ${<string>(defense?.toString())}`;

  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}
