import { createElement } from "../helpers/domHelper";
import { IFighter } from "../interfaces/IHtmlElement";
import { showModal } from "./modal";


export  function showWinnerModal(fighter: IFighter): void {
  const title: string = 'Winner';
  const bodyElement: HTMLDivElement = createWinnerDetails(fighter);

  const rootElement: HTMLElement = document.getElementById('root') as HTMLElement;
  rootElement.innerHTML = 'Game over! Reload page to start again!';
  rootElement.setAttribute('style', 'font-size:2rem');

  showModal(title, bodyElement);
}


function createWinnerDetails(fighter: IFighter): HTMLDivElement {
  const { name, health, attack, defense, source } = fighter;
  
  const fighterDetails: HTMLDivElement = createElement({ tagName: 'div', className: 'modal-body' }) as HTMLDivElement;
  
  const nameElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-name' }) as HTMLSpanElement;
  const healthElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-health' }) as HTMLSpanElement;
  const attackElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-attack' }) as HTMLSpanElement;
  const defenseElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'fighter-defense' }) as HTMLSpanElement;
  const imageElement: HTMLImageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: [{ key: "src", value: source }] }) as HTMLImageElement;
  
  let roundHealth: number = Math.round(health as number);

  nameElement.innerText = name;
  healthElement.innerText =  `Health:  ${<string>(roundHealth.toString())}`;
  attackElement.innerText =  `Attack:   ${<string>(attack?.toString())}`;
  defenseElement.innerText = `Defense: ${<string>(defense?.toString())}`;

  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}