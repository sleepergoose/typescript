import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { IFighter } from './interfaces/IHtmlElement';


const rootElement: HTMLDivElement = document.getElementById('root') as HTMLDivElement;
const loadingElement: HTMLDivElement = document.getElementById('loading-overlay') as HTMLDivElement;


export async function startApp(): Promise<void> {
  try {
    if(rootElement && loadingElement)
      loadingElement.style.visibility = 'visible';

      const fighters: IFighter[] = await getFighters() as IFighter[];
      const fightersElement: HTMLDivElement = createFighters(fighters);

      rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    loadingElement.style.visibility = 'hidden';
  }
}
