import { IHtmlElement } from '../interfaces/IHtmlElement';


export function createElement(domElement: IHtmlElement ) : HTMLElement {
  const element: HTMLElement = document.createElement(domElement.tagName);
  
  if (domElement.className) {
    element.classList.add(domElement.className);
  }

  if(domElement.attributes) {
    for(let attr of domElement.attributes) {
      element.setAttribute(attr.key, attr.value);
    }
  }
  
  return element;
}