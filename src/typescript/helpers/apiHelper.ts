import { fightersDetails, fighters } from './mockData';
import { IFighter } from '../interfaces/IHtmlElement';
import { API_URL } from '../constants/apiURL';


const useMockAPI = true;

async function callApi(endpoint: string, method: string): Promise<IFighter | IFighter[]> {
  const url: string = API_URL + endpoint;

  const options: object = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)) as IFighter | IFighter[])
        .catch((error) => {
          throw error;
        });
}


async function fakeCallApi(endpoint: string) : Promise<IFighter | IFighter[]> {
  const response: IFighter | IFighter[] = endpoint === 'fighters.json' ? fighters as IFighter[] : getFighterById(endpoint); 

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}


function getFighterById(endpoint: string): IFighter {
  const start: number = endpoint.lastIndexOf('/');
  const end: number = endpoint.lastIndexOf('.json');
  const id: string = endpoint.substring(start + 1, end);

  return (fightersDetails as IFighter[]).find((it) => it._id === id) as IFighter;
}


export { callApi };
