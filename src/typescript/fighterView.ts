import { createElement } from './helpers/domHelper'; 
import { IAttribute, IFighter, IEventHandler } from './interfaces/IHtmlElement';


export function createFighter(fighter: IFighter, handleClick: IEventHandler, selectFighter: IEventHandler): HTMLDivElement {
  const { name, source } = fighter;

  const nameElement: HTMLSpanElement = createName(name);
  const imageElement: HTMLImageElement = createImage(source);
  const checkboxElement: HTMLLabelElement = createCheckbox();
  const fighterContainer: HTMLDivElement = createElement({ tagName: 'div', className: 'fighter' }) as HTMLDivElement;
  
  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();   // type: (ev: Event) => void
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);  // type: (ev: Event) => void
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);     // type: (ev: Event) => void

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);

  return fighterContainer;
}


function createName(name: string): HTMLSpanElement {
  const nameElement: HTMLSpanElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}


function createImage(source: string): HTMLImageElement {
  const attributes: IAttribute[] = [{ key: "src", value: source }];
  const imgElement: HTMLImageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes }) as HTMLImageElement;

  return imgElement;
}


function createCheckbox(): HTMLLabelElement {
  
  const label: HTMLLabelElement = createElement({ tagName: 'label', className: 'custom-checkbox' }) as HTMLLabelElement;
  const span: HTMLSpanElement = createElement({ tagName: 'span', className: 'checkmark' }) as HTMLSpanElement;
  const attributes: IAttribute[] = [{ key: "type", value: 'checkbox' }];
  const checkboxElement: HTMLInputElement = createElement({ tagName: 'input', attributes }) as HTMLInputElement;

  label.append(checkboxElement, span);
  return label;
}



export function createFighterForStage(fighter: IFighter, cssId: string, source: string): HTMLDivElement {
  const { health } = fighter;

  const healthElement: HTMLSpanElement = createElement({tagName: 'span', 
          attributes: [{key: 'id', value:`${cssId}-health`}]}) as HTMLSpanElement;
  const imageElement: HTMLImageElement = createElement({tagName: 'img', className: 'fighter-image', 
          attributes: [{key: 'src', value: source}, {key: 'id', value: `${cssId}-image`}]}) as HTMLImageElement;
  const fighterContainer: HTMLDivElement = createElement({ tagName: 'div', className: 'fighter' }) as HTMLDivElement;
  
  healthElement.innerText = 'Health: ' + <string>health?.toString();

  fighterContainer.append(imageElement, healthElement);

  return fighterContainer;
}