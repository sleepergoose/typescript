import { IFighter } from "./interfaces/IHtmlElement";
import { createStage, hit } from "./stageView";


export async function fight(...fighters: IFighter[]): Promise<IFighter>  {
  let left: IFighter = fighters[0];
  let right: IFighter = fighters[1];
  let kickDirection: boolean = true; // from left to right
  let interval: number = 250;

  createStage(left, right);

  return await goFight(left, right, kickDirection, interval).then(response => { return response; });
}


const goFight = (left: IFighter, right: IFighter, kickDirection: boolean, interval: number): Promise<IFighter> => {
  
  const promise = new Promise<IFighter>((resolve): void => {   
    const kicker: (() => void) = () => {
      if(kickDirection){ 
        if(right.health) {
          hit('left', left.health as number);
          right.health -= getDamage(left, right);
        } 
      } else {
        if(left.health) {
          hit('right', right.health as number);
          left.health -= getDamage(right, left);
        } 
      }
      kickDirection = !kickDirection;

      if(right.health && right.health <= 0) {
        clearInterval(timerId);
        return resolve(left);
      } 
  
      if(left.health && left.health <= 0) {
        clearInterval(timerId);
        return resolve(right);
      } 
    };

    let timerId = setInterval(kicker, interval);  
  });

  clearInterval(interval);
  return promise; 
};


export function getDamage(attacker: IFighter, enemy: IFighter): number {
  let hitPower: number = getHitPower(attacker);
  let blockPower: number = getBlockPower(enemy);
  let damage: number = hitPower > blockPower ? hitPower - blockPower : 0;

  return damage 
}


export function getHitPower(fighter: IFighter): number {
  let criticalHitChance: number = Math.random() + 1;
  let attack: number = fighter.attack ? fighter.attack : 1;
  let hitPower: number = attack * criticalHitChance;

  return hitPower;
}


export function getBlockPower(fighter: IFighter): number {
  let dodgeChance: number = Math.random() + 1;
  let defense: number = fighter.defense ? fighter.defense : 1;
  let blockPpower: number = defense * dodgeChance;

  return blockPpower;
}
