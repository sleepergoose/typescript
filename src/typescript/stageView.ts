import { createFighterForStage } from "./fighterView";
import { createElement } from "./helpers/domHelper";
import { IFighter } from "./interfaces/IHtmlElement";


const hitImages: string[] = [
    '../../resources/assets/ryu.gif',
    '../../resources/assets/dhal.gif',
    '../../resources/assets/guile.gif',
    '../../resources/assets/zangief.gif',
    '../../resources/assets/ken.gif',
    '../../resources/assets/bison.gif'
  ];

  
export function createStage(left: IFighter, right: IFighter): void {
    const rootElement: HTMLElement = document.getElementById('root') as HTMLElement;
    
    const title: HTMLDivElement = createElement({tagName: 'div', className: 'stage-title',
        attributes: [{key: 'id', value: 'stage-title'}]}) as HTMLDivElement;
    const leftView: HTMLDivElement = createFighterForStage(left, 'left', hitImages[parseInt(left._id) - 1]);
    const rightView: HTMLDivElement = createFighterForStage(right, 'right', hitImages[parseInt(right._id) - 1]);
    const stage: HTMLDivElement = createElement({tagName: 'div', className: 'stage'}) as HTMLDivElement;

    title.innerText = `${left.name} vs ${right.name}`;
    stage.append(title, leftView, rightView);
    rootElement.innerHTML = '';
    rootElement.appendChild(stage);
}


export function hit(fighterPosition: string, health: number): void {
    const leftFigherHealth: HTMLSpanElement = document.getElementById('left-health') as HTMLSpanElement;
    const rightFigherHealth: HTMLSpanElement = document.getElementById('right-health') as HTMLSpanElement;

    switch (fighterPosition) {
        case 'left':
            leftFigherHealth.innerText = 'Health: ' + Math.ceil(health).toString();
            break;
        case 'right':
            rightFigherHealth.innerText = 'Health: ' + Math.ceil(health).toString();
            break;    
    }
}