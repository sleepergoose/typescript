import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IEventHandler, IFighter } from './interfaces/IHtmlElement';
import { getFighterDetails } from './services/fightersService';



export function createFighters(fighters: IFighter[]): HTMLDivElement {
  const selectFighterForBattle: IEventHandler = createFightersSelector();
  const fighterElements: HTMLDivElement[] = fighters.map(fighter => 
    createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer: HTMLDivElement = createElement({ tagName: 'div', className: 'fighters' }) as HTMLDivElement;

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}


const fightersDetailsCache = new Map<string, IFighter>();


async function showFighterDetails(event: Event, fighter: IFighter): Promise<void> {
  let fullInfo: IFighter;

  if(fightersDetailsCache.has(fighter._id)) {
    fullInfo = fightersDetailsCache.get(fighter._id) as IFighter;
  } else {
    fullInfo = await getFighterDetails(fighter._id) as IFighter;
    fightersDetailsCache.set(fullInfo._id, fullInfo);
  }
  showFighterDetailsModal(fullInfo);
}


export async function getFighterInfo(fighterId: string): Promise<IFighter> {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  let response: IFighter;

  if(fightersDetailsCache.has(fighterId)) {
    response = fightersDetailsCache.get(fighterId) as IFighter;
  } else {
    response = await getFighterDetails(fighterId) as IFighter;
    fightersDetailsCache.set(fighterId, response);
  }

  return new Promise<IFighter>((resolve, reject) =>  response ? resolve(response) : reject(Error('Failed to load')));
}


function createFightersSelector() {
  const selectedFighters = new Map<string, IFighter>()
  
  return async function selectFighterForBattle(event: Event, fighter: IFighter) {

    const fullInfo: IFighter = await getFighterInfo(fighter._id);
    
    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner: IFighter = await fight(...selectedFighters.values());
      showWinnerModal(winner);
    } 
  }
}
