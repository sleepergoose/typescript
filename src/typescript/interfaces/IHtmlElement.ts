export interface IAttribute {
  key: string;
  value: string;
}


export interface IHtmlElement {
  tagName: string;
  className?: string;
  attributes?: IAttribute[];
}


export interface IFighter {
  _id: string;
  name: string;
  health: number | null;
  attack: number | null;
  defense: number | null;
  source: string;
}


export interface IEventHandler
{
    (event: Event, fighter: IFighter): Promise<void>;
}